#!/bin/bash

NULL=/dev/null
##################### 进度指示器

function KILLPROC() {
    if kill -0 "$1"; then
        echo "$1" | xargs kill -9 >"$NULL" 2>&1
    fi
}

function PROC_NAME() {
    printf "%-45s" "$1"
    tput sc
    while true; do
        for ROATE in '-' "\\" '|' '/'; do
            tput rc && tput ed
            printf "\033[1;36m%-s\033[0m" ${ROATE}
            sleep 0.2
        done
    done
}

function CHECK_STATUS() {
    if [ $? == 0 ]; then
        KILLPROC "$1" &>"$NULL"
        tput rc && tput ed
        printf "\033[1;36m%-7s\033[0m\n" 'SUCCESS'
    else
        KILLPROC "$1" &>"$NULL"
        tput rc && tput ed
        printf "\033[1;31m%-7s\033[0m\n" 'FAILED'
    fi
}

function WORK() {
    if [ "$1" = install ]; then
        PROC_NAME "Install $2" &
        PROC_PID=$!
        yum -y install "$2" >"$NULL" 2>&1
        CHECK_STATUS ${PROC_PID}
    elif [ "$1" = remove ]; then
        PROC_NAME "Remove $2" &
        PROC_PID=$!
        yum -y remove "$2" >"$NULL" 2>&1
        CHECK_STATUS ${PROC_PID}
    elif [ "$1" = wget ]; then
        PROC_NAME "Download $3" &
        PROC_PID=$!
        wget "$2" -c -P "$download_path" >"$NULL" 2>&1
        CHECK_STATUS ${PROC_PID}
    elif [ "$1" = tar ]; then
        PROC_NAME "Extract $2" &
        PROC_PID=$!
        tar -zxf "$2" -C "$3" --strip-components 1 && echo "$(date +%F' '%H:%M:%S) $2 extrac success!,path is $3" >>"$install_log_path""$install_log"
        CHECK_STATUS ${PROC_PID}
    elif [ "$1" = make ]; then
        PROC_NAME "make $3" &
        PROC_PID=$!
        make >"$NULL" 2>&1
        make install >"$NULL" 2>&1
        CHECK_STATUS ${PROC_PID}
    fi

}

path_programming() {
    # 路径规划
    download_path=/opt/download/
    install_log_path=/opt/log/

    mkdir -p $install_log_path
    # 日志文件名
    install_log=install_elasticsearch.log
    touch ${install_log_path}$install_log

    echo "请输入绝对路径"
    read -rp "请输入软件安装位置 (/opt/apps/elasticsearch/)>" install_path
    if [ -z "$install_path" ]; then
        install_path=/opt/apps/elasticsearch/
    fi
}

# 检测路径是否存在，不存在则创建
check_path() {
    echo "目录检查"
    for path_name in "$@"; do
        [ -d "$path_name" ] || mkdir -p "$path_name" >"$NULL" 2>&1
        echo "$(date +%F' '%H:%M:%S) ${path_name} check success " >>"$install_log_path""$install_log"
    done
}

create_user() {
    echo "创建用户"
    groupadd elastic >"$NULL" 2>&1
    useradd -g elastic elastic >"$NULL" 2>&1
    chown -R elastic:elastic "$install_path"
    echo 'elastic' | passwd --stdin elastic >"$NULL"
    # su -c mysql
}

check_commend() {
    # echo "命令检查:$1"
    if rpm -qa | grep "$1" >"$NULL"; then
        echo "$(date +%F' '%H:%M:%S) check command $1 " >>"$install_log_path""$install_log"
    else
        WORK install "$1"
    fi
}

download_software() {
    # echo "下载源码包..."
    # mkdir -p $download_path
    for file in "$@"; do
        WORK wget "$file" elasticsearch
        # wget "$file" -c -P "$download_path" >"$NULL" 2>&1
        if [ $? -eq 0 ]; then
            echo "$(date +%F' '%H:%M:%S) $file download success!" >>"$install_log_path""$install_log"
        else
            echo "$(date +%F' '%H:%M:%S) $file download fail!" >>"$install_log_path""$install_log" && exit 1
        fi
    done
}

extract_file() {
    # echo "解压 $1"
    if [ ${1##*.} = gz ]; then
        WORK tar "$1" "$2"
        # tar -zxf "$1" -C "$2" --strip-components 1 && echo "$(date +%F' '%H:%M:%S) $file extrac success!,path is $2" >>"$install_log_path""$install_log"
    else
        echo "$(date +%F' '%H:%M:%S) $file type error, extrac fail!" >>"$install_log_path""$install_log" && exit 1
    fi

    # tar -zxvf "$1" -C "$extract_path"
}

start_elasticsearch() {

    # 配置java版本，由于es和jdk是一个强依赖的关系，所以当我们在新版本的ElasticSearch压缩包中包含有自带的jdk，
    # 但是当我们的Linux中已经安装了jdk之后，就会发现启动es的时候优先去找的是Linux中已经装好的jdk，
    # 此时如果jdk的版本不一致，就会造成jdk不能正常运行

    # TODO java -version
    java_version_config=$(sed -n -e '/! -x "$JAVA"/=' "$install_path"bin/elasticsearch-env)
    sed -i "${java_version_config}i\JAVA=\"\$ES_HOME/jdk/bin/java\"" "$install_path"bin/elasticsearch-env
    sed -i "${java_version_config}i\JAVA_TYPE=\"bundled jdk\"" "$install_path"bin/elasticsearch-env
    # JAVA="$ES_HOME/jdk/bin/java
    # JAVA_TYPE="bundled jdk

    if ifconfig | grep ens33 >"$NULL"; then
        local_ip=$(ifconfig ens33 | grep -w inet | awk '{print $2}')
    else
        local_ip=$(ifconfig eth0 | grep -w inet | awk '{print $2}')
    fi
    # public_ip=$(curl ifconfig.me -s | awk -F# '{print $1}')
    # 经过测试，阿里云服务器部署elasticsearch 时，ip使用私网IP才能生效
    ip=$local_ip
    # read -p "是否部署在内网 [y|n] >" in_local_net
    # case $in_local_net in
    # y | Y | yes | YES)
    #     ip=$local_ip
    #     ;;
    # n | N | NO | no)
    #     ip=$public_ip
    #     ;;
    # esac

    var1=$(sed -n -e '/network.host/=' "$install_path"config/elasticsearch.yml)
    sed -i "${var1}a\network.host: $ip" "$install_path"config/elasticsearch.yml

    # http.port: 9200
    var2=$(sed -n -e '/http.port/=' "$install_path"config/elasticsearch.yml)
    sed -i "${var2}a\http.port: 9200" ${install_path}config/elasticsearch.yml

    # discovery.seed_hosts: ["192.168.203.3", "127.0.0.1"]
    var3=$(sed -n -e '/host1/=' "$install_path"config/elasticsearch.yml)
    sed -i "${var3}a\discovery.seed_hosts: [\"${ip}\"]" ${install_path}config/elasticsearch.yml

    # 云服务器的配置比较低，需要修改jvm的参数，内存小于4G的机子，修改jvm内存
    meminfo=$(cat /proc/meminfo | grep MemTotal | awk '{print $2}')
    if [ "$meminfo" -lt 5000000 ]; then
        var4=$(sed -n -e '/-Xms/=' "$install_path"config/jvm.options)
        sed -i "${var4}d" ${install_path}config/jvm.options
        sed -i "${var4}a\-Xms256m" ${install_path}config/jvm.options

        var5=$(sed -n -e '/-Xmx/=' "$install_path"config/jvm.options)
        sed -i "${var5}d" ${install_path}config/jvm.options
        sed -i "${var5}a\-Xmx256m" ${install_path}config/jvm.options
    fi
    # cd ${install_path}bin || exit
    # su -c "./elasticsearch -d" elastic >"$NULL" 2>&1
}

is_online() {
    echo "========================================="
    echo "如果是离线安装，请将tar包放置在${download_path}路径下"
    echo "1)离线安装"
    echo "2)在线安装"
    echo "========================================="
    read -rp "请选择 > " online
}
# URL 下载地址
choice_version() {
    echo "========================================="
    echo "选择elasticsearch 安装版本"
    echo "1)elasticsearch-7.9.2"  # https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.9.2-linux-x86_64.tar.gz
    echo "2)elasticsearch-7.17.0" # https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.17.0-linux-x86_64.tar.gz
    echo "3)elasticsearch-8.1.0"  # https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.1.0-linux-x86_64.tar.gz
    echo "4)elasticsearch-8.3.3"  # https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.3.3-linux-x86_64.tar.gz
    echo "其他选项退出脚本"
    echo "========================================="
    read -rp "请选择安装版本 > " version

    case $version in
    1)
        URL=https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.9.2-linux-x86_64.tar.gz
        ;;
    2)
        URL=https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.17.0-linux-x86_64.tar.gz
        ;;
    3)
        URL=https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.1.0-linux-x86_64.tar.gz
        ;;
    4)
        URL=https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.3.3-linux-x86_64.tar.gz
        ;;
    *)
        exit 1
        ;;
    esac
}

ready() {
    cat >>/etc/sysctl.conf <<EOF
vm.max_map_count=262144
EOF
    sysctl -p >"$NULL"
    cat >>/etc/security/limits.conf <<EOF
elastic soft nofile 262144
elastic hard nofile 524288
elastic soft nproc 4096
elastic hard nproc 4096
EOF

}

main() {
    ready
    clear
    path_programming
    check_path $install_path $download_path

    is_online
    if [ "$online" -eq "1" ]; then
        echo "开始离线安装"
        if ! find $download_path | grep gz$; then
            echo "$download_path 路径下没有gz，请检查后重试"
            is_online
        fi
    elif [ "$online" -eq "2" ]; then
        echo "开始在线安装"
        choice_version
        check_commend wget
        download_software $URL
    else
        echo "输入错误，请重新操作"
        is_online
    fi

    cd "$download_path" || exit
    elasticsearch_name=$(find | grep elasticsearch | awk -F"/" '{print $2}' | awk -F- '{print $1"-"$2}')
    for filename in elasticsearch*; do
        extract_file "$filename" $install_path
    done
    create_user
    start_elasticsearch

}

main
echo "##########################################"
echo "#                                        #"
echo "#     $elasticsearch_name 安装完成      #"
echo "#    安装位置 $install_path   #"
echo "#                                        #"
echo "##########################################"
echo "创建用户:elastic,密码:elastic"
echo "模拟重新登录:sudo -i -u root"
echo "重新登录后运行启动命令: su -c \"${install_path}bin/elasticsearch -d\" elastic"
echo "项目启动较慢，请耐心等候"
# max number of threads [2048] for user [elastic] is too low, increase to at least [4096]
# echo "测试地址: $public_ip:9200 "
echo "测试地址: $local_ip:9200 "
# echo 'curl ${local_ip}:9200'
