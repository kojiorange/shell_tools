#!/bin/bash

# 路径规划

NULL=/dev/null
install_path=/opt/apps/
mkdir -p $install_path

download_path=/opt/download/
mkdir -p $download_path

extract_path=/opt/extract/
mkdir -p $extract_path

install_log_path=/opt/log/
mkdir -p $install_log_path
# 日志文件名
install_log=install.log
touch ${install_log_path}$install_log

##################### 进度指示器

function KILLPROC() {
	if kill -0 "$1"; then
		echo "$1" | xargs kill -9 >/dev/null 2>&1
	fi
}

function PROC_NAME() {
	printf "%-45s" "$1"
	tput sc
	while true; do
		for ROATE in '-' "\\" '|' '/'; do
			tput rc && tput ed
			printf "\033[1;36m%-s\033[0m" ${ROATE}
			sleep 0.2
		done
	done
}

function CHECK_STATUS() {
	if [ $? == 0 ]; then
		KILLPROC "$1" &>/dev/null
		tput rc && tput ed
		printf "\033[1;36m%-7s\033[0m\n" 'SUCCESS'
	else
		KILLPROC "$1" &>/dev/null
		tput rc && tput ed
		printf "\033[1;31m%-7s\033[0m\n" 'FAILED'
	fi
}

function WORK() {
	if [ "$1" = install ]; then
		PROC_NAME "Install $2" &
		PROC_PID=$!
		yum -y install "$2" >"$NULL" 2>&1
		CHECK_STATUS ${PROC_PID}
	elif [ "$1" = remove ]; then
		PROC_NAME "Remove $2" &
		PROC_PID=$!
		yum -y remove "$2" >"$NULL" 2>&1
		CHECK_STATUS ${PROC_PID}
	elif [ "$1" = wget ]; then
		PROC_NAME "Download $3" &
		PROC_PID=$!
		wget "$2" -c -P "$download_path" >"$NULL" 2>&1
		CHECK_STATUS ${PROC_PID}
	elif [ "$1" = tar ]; then
		PROC_NAME "Extract $2" &
		PROC_PID=$!
		tar -zxf "$2" -C "$3" && echo "$(date +%F' '%H:%M:%S) $2 extrac success!,path is $3" >>"$install_log_path""$install_log"
		CHECK_STATUS ${PROC_PID}
	elif [ "$1" = make ]; then
		PROC_NAME "make $3" &
		PROC_PID=$!
		make >/dev/null 2>&1
		make install >/dev/null 2>&1
		CHECK_STATUS ${PROC_PID}
	fi

}

#####################
# 安装nginx所需要的依赖
install_request() {
	WORK install gcc
	WORK install gcc-c++
	WORK install automake
	WORK install autoconf
	WORK install libtool
	WORK install make
	WORK install pcre
	WORK install pcre-devel
	WORK install zlib
	WORK install zlib-devel

	# wget https://www.openssl.org/source/openssl-3.0.5.tar.gz -c -P $download_path
	if ! rpm -qa | grep wget >"$NULL"; then
		WORK install wget
	fi

}

# 下载源码包
download_software() {
	OPENSSL_URL="https://www.openssl.org/source/old/1.1.1/openssl-1.1.1o.tar.gz --no-check-certificate"
	WORK wget "$OPENSSL_URL" Openssl
	# mkdir -p $download_path
	for file in "$@"; do
		WORK wget "$file" Nginx
	done
}

# 解压源码包
extract_file() {
	if [ ${1##*.} = gz ]; then
		WORK tar "$1" "$2"
	else
		echo "$(date +%F' '%H:%M:%S) $file type error, extrac fail!" >>"$install_log_path""$install_log" && exit 1
	fi
}

# 安装openssl v1
source_install_openssl() {
	cd "$extract_path"openss* || exit
	PROC_NAME config_openssl &
	PROC_PID=$!
	./config --prefix=${install_path}ssl shared zlib-dynamic enable-camellia >/dev/null 2>&1
	CHECK_STATUS ${PROC_PID}
	make depend
	WORK make Openssl
}

# 配置openssl
config_ssl() {
	# echo "PATH=/usr/local/ssl/bin:$PATH:$HOME/bin" ~/.bash_profile
	sed -i '$i PATH=/usr/local/ssl/bin:$PATH:$HOME/bin' ~/.bash_profile
	rm -f /usr/lib64/libssl.so.1.1 /usr/lib64/libcrypto.so.1.1
	ln -s "$install_path"ssl/lib/libssl.so.1.1 /usr/lib64/libssl.so.1.1
	ln -s "$install_path"ssl/lib/libcrypto.so.1.1 /usr/lib64/libcrypto.so.1.1
}

# 安装nginx
source_install_nginx() {
	cd "$extract_path"nginx* || exit

	PROC_NAME configure_nginx &
	PROC_PID=$!
	# ./configure --sbin-path="$install_path"nginx/nginx --conf-path="$install_path"nginx/nginx.conf --pid-path="$install_path"nginx/nginx.pid --with-openssl=/usr/local/ssl >/dev/null 2>&1
	./configure --prefix="$install_path"nginx --with-openssl=${install_path}ssl >/dev/null 2>&1
	CHECK_STATUS ${PROC_PID}

	WORK make Nginx
}

start_nginx() {
	cd "$install_path"nginx/sbin || exit
	if ./nginx; then
		echo "nginx 启动成功"
	else
		echo "nginx 启动失败"
	fi
}
function is_online() {
	clear
	echo "1) 如果是离线安装，请将tar包放置在${download_path}路径下"
	echo "2) 网络安装"
	echo "3) 退出脚本"
	read -rp "请选择 > " online
}

function choice_version() {
	clear
	echo "1)nginx-1.22.0 "
	echo "2)nginx-1.18.0 "
	echo "3)nginx-1.12.2 "
	read -rp "请选择nginx版本 > " nginx_version

	case $nginx_version in
	1)
		URL=https://nginx.org/download/nginx-1.22.0.tar.gz
		;;
	2)
		URL=https://nginx.org/download/nginx-1.18.0.tar.gz
		;;
	3)
		URL=https://nginx.org/download/nginx-1.12.2.tar.gz
		;;
	*)
		exit 1
		;;
	esac
}

local_install() {
	if (! find $download_path) | grep openssl; then
		echo "$download_path 路径下没有openssl源码包，请检查后重试"
		is_online
	fi
	if (! find $download_path) | grep nginx; then
		echo "$download_path 路径下没有nginx源码包，请检查后重试"
		is_online
	fi
}

ready() {
	clear
	echo "此脚本目前只提供nginx和依赖openssl离线安装，其他依赖使用的是在线安装"
}

add_service() {
	# 添加系统服务
	ln -s "$install_path"nginx /usr/local/nginx
	cat >/usr/lib/systemd/system/nginx.service <<EOF
[Unit]
Description=nginx - web server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/usr/local/nginx/logs/nginx.pid
ExecStartPre=/usr/local/nginx/sbin/nginx -t -c /usr/local/nginx/conf/nginx.conf
ExecStart=/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
ExecReload=/usr/local/nginx/sbin/nginx -s reload
ExecStop=/usr/local/nginx/sbin/nginx -s stop
ExecQuit=/usr/local/nginx/sbin/nginx -s quit
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF
	systemctl daemon-reload

}
main() {

	ready
	install_request

	is_online
	if [ "$online" == "1" ]; then
		local_install
	elif [ "$online" == "2" ]; then
		choice_version
		download_software $URL
	elif [ "$online" == "3" ]; then
		exit 1
	else
		echo "输入错误，请重新操作"
		is_online
	fi

	rm -rf ${extract_path}*
	cd "$download_path" || exit
	for filename in nginx* openssl*; do
		extract_file "$filename" $extract_path
	done
	source_install_openssl
	config_ssl
	source_install_nginx
	add_service
	# start_nginx
	systemctl start nginx.service

}

main
if ifconfig | grep ens33 >/dev/null; then
	local_ip=$(ifconfig ens33 | grep -w inet | awk '{print $2}')
else
	local_ip=$(ifconfig eth0 | grep -w inet | awk '{print $2}')
fi
public_ip=$(curl ifconfig.me -s | awk -F# '{print $1}')
echo "nginx 安装位置 ${install_path}nginx"
echo "openssl 安装位置 ${install_path}ssl"
echo "内网访问地址:$local_ip"
echo "公网访问地址:$public_ip"
