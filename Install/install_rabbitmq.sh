#!/bin/bash

# 导入证书安装依赖
ready() {
    clear
    echo "安装准备中……"
    # 完成erlang的前置条件配置
    curl -s https://packagecloud.io/install/repositories/rabbitmq/erlang/script.rpm.sh | sudo bash >/dev/null 2>&1

    # 通过rpm安装erlang
    yum install -y erlang >/dev/null 2>&1

    # 导入公钥
    rpm --import https://packagecloud.io/rabbitmq/rabbitmq-server/gpgkey >/dev/null 2>&1
    rpm --import https://packagecloud.io/gpg.key >/dev/null 2>&1
    # 完成RabbitMQ的前置条件配置
    curl -s https://packagecloud.io/install/repositories/rabbitmq/rabbitmq-server/script.rpm.sh | sudo bash >/dev/null 2>&1

    rpm --import https://www.rabbitmq.com/rabbitmq-release-signing-key.asc >/dev/null 2>&1
    yum -y install epel-release socat >/dev/null 2>&1
}

# 选择安装版本
choice_version() {
    echo "RabbitMQ 安装版本，需要根据操作系统内核选择版本"
    echo "1)RabbitMQ 3.7.28[el6]" # https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.7.28/rabbitmq-server-3.7.28-1.el6.noarch.rpm
    echo "2)RabbitMQ 3.8.5[el7]"  # https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.8.5/rabbitmq-server-3.8.5-1.el7.noarch.rpm
    echo "3)RabbitMQ 3.9.21[el8]" # https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.9.21/rabbitmq-server-3.9.21-1.el8.noarch.rpm
    echo "4)RabbitMQ 3.10.7[el8]" # https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.10.7/rabbitmq-server-3.10.7-1.el8.noarch.rpm
    echo "其他选项退出脚本"
    read -rp "请选择安装版本 > " version
    case $version in
    1)
        if uname -r | grep el6 >/dev/null; then
            URL=https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.7.28/rabbitmq-server-3.7.28-1.el6.noarch.rpm
        else
            echo "查看内核版本:uname -r"
            choice_version
        fi
        ;;
    2)
        if uname -r | grep el7 >/dev/null; then
            URL=https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.8.5/rabbitmq-server-3.8.5-1.el7.noarch.rpm
        else
            echo "查看内核版本:uname -r"
            choice_version
        fi
        ;;
    3)
        if uname -r | grep el8 >/dev/null; then
            URL=https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.9.21/rabbitmq-server-3.9.21-1.el8.noarch.rpm
        else
            echo "查看内核版本:uname -r"
            choice_version
        fi
        ;;
    4)
        if uname -r | grep el8 >/dev/null; then
            URL=https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.10.7/rabbitmq-server-3.10.7-1.el8.noarch.rpm
        else
            echo "查看内核版本:uname -r"
            choice_version
        fi
        ;;
    *)
        exit 1
        ;;
    esac
}

# 路径规划
path_programming() {

    download_path=/opt/download/
    mkdir -p $download_path

    install_log_path=/opt/log/
    mkdir -p $install_log_path
    # 日志文件名
    install_log=install_elasticsearch.log
    touch ${install_log_path}$install_log

}

download_software() {
    echo "下载rpm包..."
    if rpm -qa | grep wget >/dev/null; then
        echo "$(date +%F' '%H:%M:%S) check command wget " >>"$install_log_path""$install_log"
    else
        yum -y install wget >/dev/null 2>&1
    fi
    for file in "$@"; do
        wget "$file" -c -P "$download_path" >/dev/null 2>&1
        if [ $? -eq 0 ]; then
            echo "$(date +%F' '%H:%M:%S) $file download success!" >>"$install_log_path""$install_log"
        else
            echo "$(date +%F' '%H:%M:%S) $file download fail!" >>"$install_log_path""$install_log" && exit 1
        fi
    done
}

# 安装
install_rabbitmq() {
    cd $download_path || exit
    rpm -ivh rabbitmq* >/dev/null 2>&1
    rabbitmq-plugins enable rabbitmq_management >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        echo "$(date +%F' '%H:%M:%S) $file install success!" >>"$install_log_path""$install_log"
    else
        echo "$(date +%F' '%H:%M:%S) $file install fail!" >>"$install_log_path""$install_log" && exit 1
    fi
    # rm -f rabbitmq* >/dev/null 2>&1
}

init_rabbitmq() {
    echo "创建RabbitMQ 管理员用户"
    read -rp "username (admin)>" mq_user
    if [ -z "$mq_user" ]; then
        mq_user="admin"
    fi
    read -rp "password (admin)>" mq_password
    if [ -z "$mq_password" ]; then
        mq_password="admin"
    fi
    systemctl start rabbitmq-server
    # RabbitMQ3.3以后，guest账号只能在本机登录

    rabbitmqctl add_user "$mq_user" "$mq_password" >/dev/null 2>&1
    rabbitmqctl set_user_tags "$mq_user" administrator >/dev/null 2>&1
    rabbitmqctl set_permissions -p / "$mq_user" "." "." ".*" >/dev/null 2>&1
    systemctl restart rabbitmq-server >/dev/null 2>&1
}

main() {
    ready
    path_programming
    choice_version
    download_software $URL
    install_rabbitmq
    init_rabbitmq
    if ifconfig | grep ens33 >/dev/null; then
        local_ip=$(ifconfig ens33 | grep -w inet | awk '{print $2}')
    else
        local_ip=$(ifconfig eth0 | grep -w inet | awk '{print $2}')
    fi
    public_ip=$(curl ifconfig.me -s | awk -F# '{print $1}')
    clear
    echo "测试地址:${local_ip}:15672"
    echo "测试地址:${public_ip}:15672"
    echo "username:$mq_user"
    echo "password:$mq_password"
}

main
