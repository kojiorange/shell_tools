#!/bin/bash

groupadd dinstall
useradd -g dinstall -m -d /home/dmdba -s /bin/bash dmdba
echo 'zzzz' | passwd --stdin dmdba


mkdir -p /dm8/dmdata
mkdir -p /dm8/dmarch/arch
mkdir -p /dm8/dmbak/bak

chown -R dmdba:dinstall /dm8/


cat >> /etc/security/limits.conf <<EOF

dmdba soft nproc 10240
dmdba hard nproc 10240
dmdba soft nofile 65536
dmdba hard nofile 65536
dmdba soft core unlimited
EOF


sed -i 's/#DefaultLimitCORE/DefaultLimitCORE=infinity/g' /etc/systemd/system.conf
sed -i 's/#DefaultLimitNOFILE/DefaultLimitNOFILE=65536/g' /etc/systemd/system.conf
sed -i 's/#DefaultLimitNPROC/DefaultLimitNPROC=65536/g' /etc/systemd/system.conf

mount /opt/dm8*.iso /mnt/

cd /mnt || exit


