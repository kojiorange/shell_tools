#!/bin/bash

# 路径规划
install_path=/opt/apps/redis/
mkdir -p "$install_path"

download_path=/opt/download/
mkdir -p $download_path

install_log_path=/opt/log/
mkdir -p $install_log_path
# 日志文件名
install_log=install.log
touch ${install_log_path}$install_log

##################### 进度指示器

function KILLPROC() {
    if kill -0 "$1"; then
        echo "$1" | xargs kill -9 >/dev/null 2>&1
    fi
}

function PROC_NAME() {
    printf "%-45s" "$1"
    tput sc
    while true; do
        for ROATE in '-' "\\" '|' '/'; do
            tput rc && tput ed
            printf "\033[1;36m%-s\033[0m" ${ROATE}
            sleep 0.5
        done
    done
}

function CHECK_STATUS() {
    if [ $? == 0 ]; then
        KILLPROC "$1" &>/dev/null
        tput rc && tput ed
        printf "\033[1;36m%-7s\033[0m\n" 'SUCCESS'
    else
        KILLPROC "$1" &>/dev/null
        tput rc && tput ed
        printf "\033[1;31m%-7s\033[0m\n" 'FAILED'
    fi
}

do_work() {
    if [ "$1" = yum ]; then
        PROC_NAME "$2" &
        PROC_PID=$!
        yum -y install "$2" >/dev/null 2>&1
        CHECK_STATUS ${PROC_PID}
    elif [ "$1" = wget ]; then
        PROC_NAME "$2" &
        PROC_PID=$!
        wget "$2" -c -P "$download_path" >/dev/null 2>&1
        CHECK_STATUS ${PROC_PID}
    elif [ "$1" = tar ]; then
        PROC_NAME "extract_$2" &
        PROC_PID=$!
        tar -zxf "$2" -C "$3" --strip-components 1 && echo "$(date +%F' '%H:%M:%S) $2 extrac success!,path is $3" >>"$install_log_path""$install_log"
        CHECK_STATUS ${PROC_PID}
    fi
}

#####################
# 安装redis所需要的依赖
install_request() {

    clear
    echo "=========================install_request==========================="
    do_work yum cpp
    do_work yum binutils
    do_work yum glibc
    do_work yum glibc-kernheaders
    do_work yum glibc-common
    do_work yum glibc-devel
    do_work yum gcc
    do_work yum make
    do_work yum centos-release-scl
    do_work yum devtoolset-9-gcc
    do_work yum devtoolset-9-gcc-c++
    do_work yum devtoolset-9-binutils
    echo "source /opt/rh/devtoolset-9/enable" >>/etc/profile
    # scl enable devtoolset-9 bash

    # wget https://www.openssl.org/source/openssl-3.0.5.tar.gz -c -P $download_path
    if ! rpm -qa | grep wget; then
        do_work yum wget
    fi
}

# 下载源码包
download_software() {
    echo "=========================download_software=========================="
    for file in "$@"; do
        do_work wget "$file"
    done
}

# 解压源码包
extract_file() {
    echo "===========================extract_file============================="
    if [ ${1##*.} = gz ]; then
        do_work tar "$1" "$2"
    else
        echo "$(date +%F' '%H:%M:%S) $1 type error, extrac fail!" >>"$install_log_path""$install_log" && exit 1
    fi
}

make_redis() {
    echo "====================== make && install redis========================"
    cd "$install_path" || exit
    PROC_NAME make_redis &
    PROC_PID=$!
    make MALLOC=libc >/dev/null 2>&1
    cd src && make install >/dev/null 2>&1
    CHECK_STATUS ${PROC_PID}
}

start_redis() {
    cd "$install_path" || exit
    if (src/redis-server "$install_path"redis.conf); then
        echo "redis start success"
    else
        echo "redis start fail!"
    fi
}

set_passwd() {
    read -rp "是否需要密码 [y|n]:" have_passwd
    case $have_passwd in
    y | Y | yes | YES | Yes)
        stty -echo # 关闭用户输入显示
        read -rp "请输入密码:" passwd
        read -rp "确认密码:" check_passwd
        stty echo # 开启用户输入显示
        if [ "$passwd" -ne "$check_passwd" ]; then
            clear
            echo "两次输入不一致"
            set_passwd
        else
            sed -i "s/^# requirepass foobared/requirepass $passwd/g" "$install_path"redis.conf
        fi
        ;;
    n | N | NO | no | No | nO) ;;

    *) set_passwd ;;
    esac
}

conf_redis() {
    set_passwd
    read -rp "配置redis端口(4236):" redis_port
    if [ -n "$redis_port" ];then
        sed -i "s/^port 6379/port $redis_port/g" "$install_path"redis.conf
    fi
    
    sed -i "s/^bind 127.0.0.1/bind 0.0.0.0/g" "$install_path"redis.conf
    sed -i "s/^protected-mode yes/protected-mode no/g" "$install_path"redis.conf
    sed -i "s/daemonize no/daemonize yes/g" "$install_path"redis.conf
}

function is_online() {
    clear
    echo "1) 如果是离线安装，请将tar包放置在${download_path}路径下"
    echo "2) 网络安装"
    echo "3) 退出脚本"
    read -rp "请选择 > " online
}

input_redis_version() {
    read -rp "请输入版本号(6.0.5) >" redis_version
    if [ -z "$redis_version" ]; then
        URL=https://download.redis.io/releases/redis-6.0.5.tar.gz
    else
        URL=https://download.redis.io/releases/redis-"$redis_version".tar.gz
    fi
}

function choice_version() {

    clear
    echo "1)redis-7.0.4 "
    echo "2)redis-6.0.15 "
    echo "3)redis-5.0.4 "
    echo "4)输入版本号"
    read -rp "请选择redis版本 > " redis_version

    case $redis_version in
    1)
        URL=https://download.redis.io/releases/redis-7.0.4.tar.gz
        ;;
    2)
        URL=https://download.redis.io/releases/redis-6.0.16.tar.gz
        ;;
    3)
        URL=https://download.redis.io/releases/redis-5.0.4.tar.gz
        ;;
    4)
        input_redis_version
        ;;
    *)
        exit 1
        ;;
    esac
}

main() {

    install_request

    is_online
    if [ "$online" -eq "1" ]; then
        if ! find $download_path | grep gz; then
            echo "$download_path 路径下没有gz包，请检查后重试"
            is_online
        fi
    elif [ "$online" -eq "2" ]; then
        choice_version
        download_software $URL
    elif [ "$online" -eq "3" ]; then
        exit 1
    else
        echo "输入错误，请重新操作"
        is_online
    fi

    cd "$download_path" || exit
    for filename in redis*; do
        extract_file "$filename" $install_path
    done
    # sudo -i -u root
    # shellcheck source=/etc/profile
    source /etc/profile
    make_redis
    conf_redis
    start_redis
}

main
if ifconfig | grep ens33 >/dev/null; then
	local_ip=$(ifconfig ens33 | grep -w inet | awk '{print $2}')
else
	local_ip=$(ifconfig eth0 | grep -w inet | awk '{print $2}')
fi
public_ip=$(curl ifconfig.me -s | awk -F# '{print $1}')
echo "安装位置:$install_path"
echo "内网IP:$local_ip"
echo "公网IP:$public_ip"
echo "Password:$passwd"
