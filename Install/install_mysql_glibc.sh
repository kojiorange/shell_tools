#!/bin/bash
NULL=/dev/null
##################### 进度指示器

function KILLPROC() {
    if kill -0 "$1"; then
        echo "$1" | xargs kill -9 >"$NULL" 2>&1
    fi
}

function PROC_NAME() {
    printf "%-45s" "$1"
    tput sc
    while true; do
        for ROATE in '-' "\\" '|' '/'; do
            tput rc && tput ed
            printf "\033[1;36m%-s\033[0m" ${ROATE}
            sleep 0.5
        done
    done
}

function CHECK_STATUS() {
    if [ $? == 0 ]; then
        KILLPROC "$1" &>"$NULL"
        tput rc && tput ed
        printf "\033[1;36m%-7s\033[0m\n" 'SUCCESS'
    else
        KILLPROC "$1" &>"$NULL"
        tput rc && tput ed
        printf "\033[1;31m%-7s\033[0m\n" 'FAILED'
    fi
}

WORK() {
    if [ "$1" = install ]; then
        PROC_NAME "Install $2" &
        PROC_PID=$!
        yum -y install "$2" >"$NULL" 2>&1
        CHECK_STATUS ${PROC_PID}
    elif [ "$1" = remove ]; then
        PROC_NAME "Remove $2" &
        PROC_PID=$!
        yum -y remove "$2" >"$NULL" 2>&1
        CHECK_STATUS ${PROC_PID}
    elif [ "$1" = xf ]; then
        PROC_NAME "Extract $2" &
        PROC_PID=$!
        tar -xf "$2" -C "$3" --strip-components 1 && echo "$(date +%F' '%H:%M:%S) $2 extrac success!,path is $3" >>"$install_log_path""$install_log"
        CHECK_STATUS ${PROC_PID}
    elif [ "$1" = zxf ]; then
        PROC_NAME "Extract $2" &
        PROC_PID=$!
        tar -zxf "$2" -C "$3" --strip-components 1 && echo "$(date +%F' '%H:%M:%S) $2 extrac success!,path is $3" >>"$install_log_path""$install_log"
        CHECK_STATUS ${PROC_PID}
    elif [ "$1" = wget ]; then
        PROC_NAME "Download $2" &
        PROC_PID=$!
        wget "$2" -c -P "$download_path" >"$NULL" 2>&1
        CHECK_STATUS ${PROC_PID}
    fi
}

#####################
path_programming() {
    # 路径规划
    download_path=/opt/download/
    extract_path=/opt/extract/
    install_log_path=/opt/log/mysql/

    mkdir -p $install_log_path
    # 日志文件名
    install_log=install_mysql8.log
    touch ${install_log_path}"$install_log"
    # # 传入内容,格式化内容输出,可以传入多个参数,用空格隔开
    # echo() {
    #     for msg in "$@"; do
    #         action "$msg" /bin/true
    #     done
    # }
    echo "请输入绝对路径"
    read -rp "请输入软件安装位置 (/opt/apps/mysql/)>" install_path
    if [ -z "$install_path" ]; then
        install_path=/opt/apps/mysql/
    fi

}

# 检测路径是否存在，不存在则创建
check_path() {
    echo "目录检查"
    for path_name in "$@"; do
        [ -d "$path_name" ] || mkdir -p "$path_name" >"$NULL" 2>&1
        echo "$(date +%F' '%H:%M:%S) ${path_name} check success " >>"$install_log_path""$install_log"
    done
}

check_commend() {
    echo "命令检查:$1"
    if rpm -qa | grep "$1" >$NULL; then
        echo "$(date +%F' '%H:%M:%S) check command $1 " >>"$install_log_path""$install_log"
    else
        WORK install "$1"
        # yum -y install "$1" >"$NULL" 2>&1
    fi
}

download_software() {
    echo "下载源码包..."
    # mkdir -p $download_path
    for file in "$@"; do
        # wget "$file" -c -P "$download_path" >"$NULL" 2>&1
        WORK wget "$file"
        if [ $? -eq 0 ]; then
            echo "$(date +%F' '%H:%M:%S) $file download success!" >>"$install_log_path""$install_log"
        else
            echo "$(date +%F' '%H:%M:%S) $file download fail!" >>"$install_log_path""$install_log" && exit 1
        fi
    done
}

extract_file() {
    echo "解压 $1"
    if [ ${1##*.} = tar ]; then
        tar -xf "$1" -C "$2" && echo "$(date +%F' '%H:%M:%S) $file extrac success!,path is $2" >>"$install_log_path""$install_log"
    # elif [ $(echo $1 | awk -F. '{print $NF}') == "gz" ]; then
    elif [ ${1##*.} = xz ]; then
        WORK xf "$1" "$2"
        # tar -xf "$1" -C "$2" --strip-components 1 && echo "$(date +%F' '%H:%M:%S) $1 extrac success!,path is $2" >>"$install_log_path""$install_log"
    elif [ ${1##*.} = gz ]; then
        WORK zxf "$1" "$2"
        # tar -zxf "$1" -C "$2" --strip-components 1 && echo "$(date +%F' '%H:%M:%S) $1 extrac success!,path is $2" >>"$install_log_path""$install_log"
    else
        echo "$(date +%F' '%H:%M:%S) $file type error, extrac fail!" >>"$install_log_path""$install_log" && exit 1
    fi

    # tar -zxvf "$1" -C "$extract_path"
}

create_user() {
    echo "创建用户"
    groupadd mysql
    useradd -r -g mysql -s /bin/false mysql
    chown -R mysql:mysql "$install_path"
    # echo 'zzzz' | passwd --stdin mysql
    # su -c mysql
}

conf_mysql() {
    echo "配置MySQL"
    cat >/etc/my.cnf <<EOF
[mysqld]
basedir = $install_path
datadir = ${install_path}data
log-error = ${install_path}mysqld.log
port = 3306
socket = /tmp/mysql3306.sock
#skip-grant-tables = 1
slow_query_log = 1
slow_query_log_file = slow.log
character_set_server=utf8
max_connections=500
default_storage_engine= InnoDB
init_connect='SET collation_connection = utf8_general_ci'
init_connect='SET NAMES utf8'
character-set-server=utf8
collation-server=utf8_general_ci
skip-character-set-client-handshake
max_allowed_packet=1000000000
[client]
socket = /tmp/mysql3306.sock
EOF

    echo -e "export PATH=${install_path}bin:\$PATH" >>/etc/profile
    source /etc/profile
    # sed -i '$i export PATH=/usr/local/mysql/bin:$PATH' /etc/profile
}

init_mysql() {
    echo "初始化MySQL"
    cd "$install_path"bin || exit
    ./mysqld --user=mysql --basedir="$install_path" --datadir="$install_path"data/ --initialize
    # mysqladmin -uroot -p旧密码 password zzzz
}

add_mysql_service() {
    echo "将MySQL添加至服务管理器"
    cd "$install_path" || exit
    cp support-files/mysql.server /etc/init.d/mysql
    chkconfig --add /etc/init.d/mysql
    chkconfig mysql on
}

set_mysql_passwd() {
    read -rsp "请设置MySQL登录密码(不回显) >" mysql_passwd
    echo ""
    read -rsp "确认密码 >" check_passwd
    if [ "$mysql_passwd" != "$check_passwd" ]; then
        echo "两次输入不一致!!"
        set_mysql_passwd
    fi
}

start_mysql() {
    echo "启动MySQL"
    cd "$install_path"bin || exit
    systemctl start mysql
    # ./mysqld_safe --user=mysql &
    pass=$(sed -n '/root@localhost/p' "$install_path"/mysqld.log | awk -F "root@localhost: " '{print $NF}')
    set_mysql_passwd

    if [ "$mysql_big_version" = "5" ]; then
        # echo "MySQL5"
        mysql --connect-expired-password --ssl-mode=DISABLED -uroot -p"${pass}" -e "
set password=password(\"${mysql_passwd}\");
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '${mysql_passwd}' WITH GRANT OPTION;
FLUSH PRIVILEGES;
quit
"
    else
        # echo "MySQL8"
        mysql --connect-expired-password --ssl-mode=DISABLED -uroot -p"${pass}" -e "
use mysql;
update user set host = '%' where user ='root';
GRANT ALL ON *.* TO 'root'@'%';
flush privileges;

ALTER USER 'root'@'%' IDENTIFIED BY '${mysql_passwd}' PASSWORD EXPIRE NEVER;
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '${mysql_passwd}';
FLUSH PRIVILEGES;
quit
"
    fi
}

# 安装前准备
ready() {
    echo "正在准备安装MySQL"
    sed -i s/enforcing/disabled/g /etc/sysconfig/selinux
    systemctl restart firewalld
    if ! firewall-cmd --list-port | grep 3306 >"$NULL"; then
        firewall-cmd --add-port=3306/tcp --permanent >"$NULL" 2>&1
        firewall-cmd --reload >"$NULL"
    fi
    WORK install libaio
    WORK remove *mariadb*
    # yum -y install libaio >"$NULL"
    # yum -y remove *mariadb* >"$NULL" 2>&1

    cat >>/etc/sysctl.conf <<EOF
kernel.sem = 500 1024000 200 4096
kernel.sysrq = 1
kernel.core_uses_pid = 1
kernel.msgmnb = 65536
kernel.msgmax = 65536
kernel.msgmni = 2048
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_tw_recycle = 1
net.ipv4.tcp_max_syn_backlog = 4096
net.ipv4.ip_forward = 0
net.ipv4.conf.default.accept_source_route = 0
net.ipv4.ip_local_port_range = 1025 65535
net.core.netdev_max_backlog = 10000
net.ipv4.conf.all.arp_filter = 1
net.core.rmem_max = 2097152
net.core.wmem_max = 2097152

vm.swappiness=0
EOF

    cat >>/etc/security/limits.conf <<EOF
mysql soft nproc 65535
mysql hard nproc 65535
mysql soft nofile 65535
mysql hard nofile 65535
EOF
}

is_online() {
    echo "如果是离线安装，请将tar包放置在${download_path}路径下"
    echo "1)离线安装"
    echo "2)在线安装"
    read -rp "请选择 > " online
}
# URL 下载地址
choice_version() {
    echo "选择MySQL 安装版本"
    echo "1)mysql-5.7.9"  # https://downloads.mysql.com/archives/get/p/23/file/mysql-5.7.9-linux-glibc2.5-x86_64.tar
    echo "2)mysql-5.7.38" # https://downloads.mysql.com/archives/get/p/23/file/mysql-5.7.38-linux-glibc2.12-x86_64.tar
    echo "3)mysql-8.0.11" # https://downloads.mysql.com/archives/get/p/23/file/mysql-8.0.11-linux-glibc2.12-x86_64.tar
    echo "4)mysql-8.0.20" # https://downloads.mysql.com/archives/get/p/23/file/mysql-8.0.20-linux-glibc2.12-x86_64.tar
    echo "其他选项退出脚本"
    read -rp "请选择安装版本 >" mysql_version

    case $mysql_version in
    1)
        URL=https://downloads.mysql.com/archives/get/p/23/file/mysql-5.7.9-linux-glibc2.5-x86_64.tar
        ;;
    2)
        URL=https://downloads.mysql.com/archives/get/p/23/file/mysql-5.7.38-linux-glibc2.12-x86_64.tar
        ;;
    3)
        URL=https://downloads.mysql.com/archives/get/p/23/file/mysql-8.0.11-linux-glibc2.12-x86_64.tar
        ;;
    4)
        URL=https://downloads.mysql.com/archives/get/p/23/file/mysql-8.0.20-linux-glibc2.12-x86_64.tar
        ;;
    *)
        exit 1
        ;;
    esac
}

main() {
    clear
    ready
    path_programming
    check_path $download_path $extract_path $install_path
    is_online
    if [ "$online" = "1" ]; then
        echo "开始离线安装"
        if ! find $download_path | grep tar$; then
            echo "$download_path 路径下没有tar，请检查后重试"
            is_online
        fi
    elif [ "$online" = "2" ]; then
        echo "开始在线安装"
        choice_version
        check_commend wget
        download_software $URL
    else
        echo "输入错误，请重新操作"
        is_online
    fi

    cd "$download_path" || exit
    mysql_name=$(find . | grep tar$ | awk -F"/" '{print $2}' | awk -F- '{print $1"-"$2}')
    for filename in mysql*; do
        extract_file "$filename" $extract_path
    done

    cd $extract_path || exit
    rm -f mysql-test*
    rm -f mysql-router*
    mysql_big_version=$(echo "$mysql_name" | awk -F- '{print $2}' | awk -F. '{print $1}')
    if [ "$mysql_big_version" = "5" ]; then
        for filename in *.gz; do
            extract_file "$filename" $install_path
        done
    else
        for filename in *.xz; do
            extract_file "$filename" $install_path
        done
    fi

    create_user
    conf_mysql
    init_mysql
    add_mysql_service
    start_mysql

}

main
echo "##########################################"
echo "#                                        #"
echo "#         $mysql_name 安装完成          #"
echo "#                                        #"
echo "##########################################"
echo "安装位置 $install_path"
echo "数据存储位置 ${install_path}data"
echo "日志存储位置 ${install_path}mysqld.log"
echo "socket日志存储位置 /tmp/mysql3306.sock"
echo "启动服务 systemctl start mysql"
echo "关闭服务 systemctl stop mysql"
echo "查看服务 systemctl status mysql"
echo "刷新环境变量 source /etc/profile"
echo "初始密码为<$pass> 已重置为<$mysql_passwd>"
