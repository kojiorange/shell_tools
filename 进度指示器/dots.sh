#!/bin/bash
function dots(){
seconds=${1:-5} # print a dot every 5 seconds by default
while true
do
    sleep "$seconds"
    echo -n '.'
done
}
 
dots 7
BG_PID=$!
trap "kill -9 $BG_PID" INT
 
# Do the real job here
# yum -y install libtool >/dev/null 2>&1
sleep 150
kill $BG_PID
echo