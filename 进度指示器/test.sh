#!/bin/bash

# cat >>/etc/sysctl.conf <<EOF

# vm.panic_on_oom = 1
# vm.overcommit_memory = 1
# EOF
# sysctl -p

install_path=/opt/apps/redis/
cd "$install_path" || exit
if (src/redis-server "$install_path"redis.conf); then
    echo "redis start success"
else
    echo "redis start fail!"
fi
