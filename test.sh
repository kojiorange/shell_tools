#!/bin/bash
# by oToGamez
# www.pro-toolz.net

#  $E 换行输出
#  $e 不换行输出
      E='echo -e';
      e='echo -en';
      trap "R;exit" 2
      # 调整菜单数量 = LM+1
        LM=6
    ESC=$( $e "\e")
# 将光标移至${1},${2}
   TPUT(){ $e "\e[${1};${2}H";}
#    清理窗口
  CLEAR(){ $e "\ec";}
#   复原光标位置
  CIVIS(){ $e "\e[?25l";}
#   替换字符
   DRAW(){ $e "\e%@\e(0";}
# 取消字符替换 x |
  WRITE(){ $e "\e(B";}
#   显示背景色为白色
   MARK(){ $e "\e[7m";}
#    取消白色背景色
 UNMARK(){ $e "\e[27m";}
#  清理窗口，修改前景色和背景色 J清屏
      R(){ CLEAR ;stty sane;$e "\ec\e[37;40m\e[J";};
   HEAD(){ DRAW # 开启字符替换
           for each in $(seq 1 13);do
                $E "   x                                          x"
           done
           WRITE; # 取消字符替换
           MARK;  # 设置白色背景
           TPUT 1 5 # 将光标移到 (1,5)
           $E "BASH SELECTION MENU                       ";
           UNMARK; # 取消白色背景
           }
           i=0; CLEAR;# 清屏
        CIVIS; # 复原光标位置
        NULL=/dev/null
   FOOT(){ MARK; # 设置白色背景
        TPUT 13 5
        printf "ENTER - SELECT,NEXT                       ";
        UNMARK; # 取消白色背景
        }
# read -s 输入时不显示 -n3 限制输入长度为3 key 存储输入的值
# 读取箭头
  ARROW(){ 
        read -s -n3 key 2>/dev/null >&2
        # \033[nA 光标上移n行 
        # \033[nB 光标下移n行 
        if [[ $key = $ESC[A ]];then echo up;fi
        if [[ $key = $ESC[B ]];then echo dn;fi;
        }

# 显示的内容
     M0(){ TPUT  4 20; $e "Login info";}
     M1(){ TPUT  5 20; $e "Network";}
     M2(){ TPUT  6 20; $e "Disk";}
     M3(){ TPUT  7 20; $e "Routing";}
     M4(){ TPUT  8 20; $e "Time";}
     M5(){ TPUT  9 20; $e "ABOUT  ";}
     M6(){ TPUT 10 20; $e "EXIT   ";}

# 显示菜单 M"$each" in M1,M2,M3……
   MENU(){ for each in $(seq 0 $LM);do M"$each";done;}

# 光标指向的菜单选项
    POS(){ if [[ $cur == up ]];then ((i--));fi
           if [[ $cur == dn ]];then ((i++));fi
        #    循环选择菜单
           if [[ $i -lt 0   ]];then i=$LM;fi
           if [[ $i -gt $LM ]];then i=0;fi;
        }

# 刷新 
REFRESH(){ after=$((i+1)); before=$((i-1))
           if [[ $before -lt 0  ]];then before=$LM;fi
           if [[ $after -gt $LM ]];then after=0;fi
           if [[ $j -lt $i      ]];then UNMARK;M$before;else UNMARK;M$after;fi
           if [[ $after -eq 0 ]] || [ $before -eq $LM ];then
           UNMARK; M$before; M$after;fi;j=$i;UNMARK;M$before;M$after;}

# 初始化，打印菜单框架          
   INIT(){ R;HEAD;FOOT;MENU;}
#    刷新，设置白色背景
     SC(){ REFRESH;MARK;$S;$b;cur=$(ARROW);}
     ES(){ MARK;$e "ENTER = main menu ";$b;read;INIT;};

INIT # 清屏，打印菜单
while true; do 
case $i in
        0) S=M0;SC;if [[ $cur == "" ]];then R;$e "\n$(w        )\n";ES;fi;;
        1) S=M1;SC;if [[ $cur == "" ]];then R;$e "\n$(ifconfig )\n";ES;fi;;
        2) S=M2;SC;if [[ $cur == "" ]];then R;$e "\n$(df -h    )\n";ES;fi;;
        3) S=M3;SC;if [[ $cur == "" ]];then R;$e "\n$(route -n )\n";ES;fi;;
        4) S=M4;SC;if [[ $cur == "" ]];then R;$e "\n$(date     )\n";ES;fi;;
        5) S=M5;SC;if [[ $cur == "" ]];then R;$e "\n$($e by oTo)\n";ES;fi;;
        6) S=M6;SC;if [[ $cur == "" ]];then R;exit 0;fi;;
 esac;
 POS;done