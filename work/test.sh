#!/bin/bash

# 作者：张程
# 时间：2022/8/1
# 作用：脚本用于备份程序文件，添加到定时任务后，每天执行以此，无论成功失败都发送邮件提醒

# 邮件配置文件：/etc/mail.rc
# 定时任务文件：/etc/crontab

# 记录当前路径
sc_path=$(pwd)
# 安装mailx 终端邮件工具

        installed_mailx=$(rpm -qa | grep mailx)
        if [[ -z $installed_mailx ]];then
                yum -y install mailx &>/dev/null
        fi
        read -p "是否需要配置系统邮件<mailx>(首次运行此脚本请输入y) (n/y)> " email_conf
        if [ "$email_conf" == "y" ]
        then
                read -p "请设置系统邮件(发件人<QQ邮箱>) > " local_email
                read -p "请输入系统邮件授权码(不是邮箱密码,需要生成) > " email_password
                read -p "请输入安全证书存放路径 (/etc/pki/nssdb)> " certificate
                # 对方收到邮件时显示的发件人(昵称)
                sed -i '/from/d' /etc/mail.rc
                echo "set from=$local_email" >> /etc/mail.rc

                # smtp的认证方式，默认就是login，也可以改成CRAM-MD5或PLAIN方式
                sed -i '/smtp-auth/d' /etc/mail.rc
                echo "set smtp-auth=login" >> /etc/mail.rc

                # 邮箱账号
                sed -i '/smtp-auth-user/d' /etc/mail.rc
                echo "set smtp-auth-user=$local_email" >> /etc/mail.rc

                # 邮箱的授权码，这个不是邮箱的密码，授权码是需要自己在邮箱的设置中添加的
                sed -i '/smtp-auth-password/d' /etc/mail.rc
                echo "set smtp-auth-password=$email_password" >> /etc/mail.rc

                # 严格验证
                sed -i '/ssl-verify/d' /etc/mail.rc
                echo "set ssl-verify=strict" >> /etc/mail.rc

                # 证书文件目录
                if [ -z $certificate ]
                then
                        certificate="/etc/pki/nssdb"
                fi
                sed -i '/nss-config-dir/d' /etc/mail.rc
                echo "set nss-config-dir=$certificate" >> /etc/mail.rc

                # 配置邮箱服务器地址与证书
                if [[ $local_email =~ "@qq" ]]
                then
                        # 邮箱服务器地址，这里也可以是smtp 也可以是smtps
                        sed -i '/smtp=smtps/d' /etc/mail.rc
                        echo "set smtp=smtps://smtp.qq.com:465" >> /etc/mail.rc
                        crts=$(ls $certificate | grep qq)
                        if [[ -z $crts ]]
                        then
                                # 获取证书
                                echo   -n " " |  openssl s_client -connect smtp.qq.com:465 | sed -ne  '/-BEGIN CERTIFICATE-/,/-END CERTIFIICATE-/p'  >  $certificate/qq.crt
                                # 添加信任列表
                                certutil    -A    -n   'qq'    -t    "P,P,P"    -d    $certificate    -i    $certificate/qq.crt
                        fi
                elif [[ $local_email =~ "@139" ]]
                then
                        sed -i '/smtp=smtps/d' /etc/mail.rc
                        echo "set smtp=smtps://smtp.qq.com:465" >> /etc/mail.rc
                fi
        fi

        read -p "请设置系统系统管理员邮件(收件人) > " user_email
        read -p "请输入需要备份的程序地址(绝对路径) > " soft_path
        read -p "将程序备份到(不要放在/opt下，绝对路径) > " back_path
        read -p "请输入备份盘挂载路径(用于存储备份文件) > " back_mount_path
        mkdir -p $back_path
        mkdir -p $back_mount_path

        # 获取备份程序名
        cd $soft_path
        soft_name=$(basename "$PWD")

        # 将以下变量添加为全局变量
        echo '#!/bin/bash' > /opt/timeback.sh
        export NOW=`date +%Y-%m-%d`
        export SOFT_NAME=$soft_name
        export SOFT_PATH=$soft_path
        export BACK_PATH=$back_path
        export BACK_MOUNT_PATH=$back_mount_path
        export USER_EMAIL=$user_email
        echo -e "\n\n######################################任务一###########################################" >> /opt/timeback.sh
        # 备份文件
        echo 'cd ${SOFT_PATH}/.. && tar -zcf ${SOFT_NAME}_${NOW}.tar.gz ${SOFT_NAME}' >> /opt/timeback.sh
        echo 'mv ${SOFT_NAME}_* ${BACK_PATH} && cp ${BACK_PATH}/${SOFT_NAME}_${NOW}.tar.gz $BACK_MOUNT_PATH/' >> /opt/timeback.sh
        echo 'if [ $? -eq "0" ];then' >> /opt/timeback.sh
        echo 'echo "${NOW} 程序备份成功 备份在${BACK_PATH} 路径下" | mailx -s "备份成功" $USER_EMAIL' >> /opt/timeback.sh
        echo 'else' >> /opt/timeback.sh
        echo 'echo "${NOW} 程序备份失败 " | mailx -s "备份失败" $USER_EMAIL' >> /opt/timeback.sh
        echo 'fi' >> /opt/timeback.sh

        echo -e "\n\n######################################任务二###########################################" >> /opt/timeback.sh
        # 删除5天前的所有文件(只删除了老硬盘的备份，新增的挂载盘没有删除)
        # echo 'find ${BACK_MOUNT_PATH} -name "*" -ctime +5 -exec rm -rfv {} \;' >> /opt/timeback.sh
        echo 'find ${BACK_PATH} -name "*" -ctime +5 -exec rm -rfv {} \;' >> /opt/timeback.sh
        echo 'if [ $? -eq "0" ];then' >> /opt/timeback.sh
        echo 'echo "在${BACK_PATH} 目录下，五天前的备份已被删除" | mailx -s "备份删除成功" $USER_EMAIL' >> /opt/timeback.sh
        echo 'else' >> /opt/timeback.sh
        echo 'echo "在${BACK_PATH} 目录下，五天前的备份已被删除失败" | mailx -s "备份删除失败" $USER_EMAIL' >> /opt/timeback.sh
        echo 'fi' >> /opt/timeback.sh

        echo -e "\n\n######################################任务三###########################################" >> /opt/timeback.sh
        #Tomcat服务启动脚本
        #1、检测Tomcat服务状态为停止，自动启动Tomcat服务；
        #2、成功、失败发送邮件进行提醒。
        # 获取tomcat 进程id
        echo 'tomcat_status=$(ps -ef |grep tomcat |grep -w "tomcat"|grep -v "grep"|awk "{print $2}")' >> /opt/timeback.sh
        sed -i $'s/"{print $2}"/\'{print $2}\'/g' /opt/timeback.sh
        # 判断，如果tomcat已经启动，则不执行操作，若tomcat没有启动则启动tomcat
        echo 'if [[ -z $tomcat_status ]];then' >> /opt/timeback.sh

                echo '${SOFT_PATH}/../../bin/startup.sh &>/dev/null' >> /opt/timeback.sh
                echo 'if [ $? -eq "0" ];then' >> /opt/timeback.sh
                echo 'echo "tomcat 启动成功" | mailx -s "tomcat启动成功" $USER_EMAIL' >> /opt/timeback.sh
                echo 'else' >> /opt/timeback.sh
                echo 'echo "tomcat 启动失败" | mailx -s "tomcat启动失败" $USER_EMAIL' >> /opt/timeback.sh
                echo 'fi' >> /opt/timeback.sh
        echo 'fi' >> /opt/timeback.sh

        # 添加定时任务
        sed -i '/timeback/d' /etc/crontab
        echo '30 03 * * * root sh /opt/timeback.sh' >> /etc/crontab

        # 运行备份脚本，进行首次备份
        chmod +x /opt/timeback.sh && /opt/timeback.sh
        echo "定时任务添加成功，将会在每天凌晨3:30分对程序进行备份，并且立即完成了一次备份，使用命令 tail -1 /etc/crontab 查看 邮件已经发送到$user_email"
cd $sc_path
